package com.example.koss.contentserver;

import java.util.Observable;

/**
 * Created by koss on 18.10.2016.
 */

public class ObservableTesting extends Observable {
    public void changeSomething() {
        setChanged();
        notifyObservers();
    }
}
