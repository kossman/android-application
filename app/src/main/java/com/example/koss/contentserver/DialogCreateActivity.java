package com.example.koss.contentserver;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class DialogCreateActivity extends AppCompatActivity {

    protected Integer receiver_ID;
    protected SQLiteDB connect_DB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog_create);
        connect_DB = new SQLiteDB(this);
    }

    public void startDialog (View view) {
        EditText receiver_ID_editText = (EditText) findViewById(R.id.receiverIDfield);
        try {
            receiver_ID = Integer.parseInt(receiver_ID_editText.getText().toString());
        }
        catch (NumberFormatException e) {
            receiver_ID_editText.setError("Invalid ID number");
            return;
        }
        if (connect_DB.checkReceiverID(receiver_ID)) {
            receiver_ID_editText.setError("You already have a chat with this user");
            return;
        }
        Intent dialogOpen = new Intent(DialogCreateActivity.this, DialogActivity.class);
        dialogOpen.putExtra("activity","DialogCreateActivity");
        dialogOpen.putExtra("receiver_ID_fk", receiver_ID.toString());
        startActivity(dialogOpen);
    }
}
