package com.example.koss.contentserver;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.SQLClientInfoException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Observer;

public class DialogActivity extends AppCompatActivity {

    protected TextView dialogTextView;
    protected String activity_from;
    protected Integer receiver_ID_fk;
    protected Integer chat_ID;
    protected ListView dialogList;
    protected EditText messageInput;
    protected ArrayList messages;
    protected ImageView sendMessageButton;
    protected Integer from_me_flag;
    protected SimpleAdapter sAdapter;
    protected SQLiteDB connect_DB;
    protected String receiver_name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);
        connect_DB = new SQLiteDB(this);
        Intent intent = getIntent();
        activity_from = intent.getStringExtra("activity");
        receiver_ID_fk = intent.getIntExtra("receiver_ID_fk", 0); // receiver_ID_fk = 0 если ключ не найден в Extra
        chat_ID = intent.getIntExtra("chat_ID", 0); // chat_ID = 0 если ключ не найден в Extra
        receiver_name = intent.getStringExtra("name");
        messageInput = (EditText) findViewById(R.id.textInputDialog);
        dialogTextView = (TextView) findViewById(R.id.dialogTextView);
        dialogTextView.setText(getResources().getString(R.string.dialogHeader) + " " + receiver_name);
        dialogList = (ListView) findViewById(R.id.dialogListHistory);
        sendMessageButton = (ImageView) findViewById(R.id.sendButton);

        if (activity_from.equals("DialogCreateActivity")) {
            messages = new ArrayList<>();
        }
        else {
            messages = connect_DB.getAllMessagesChat(chat_ID);
            String[] from = {SQLiteDB.MESSAGE_TEXT_MESSAGE, SQLiteDB.MESSAGE_TIME, SQLiteDB.MESSAGE_FROM_ME};
            int[] to = {R.id.receiver_name_adapter, R.id.last_message_adapter};

            sAdapter = new MapAdaptor(this, messages, R.layout.chat_view_item,
                    from, to);

            dialogList.setAdapter(sAdapter);
        }

    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);
    }

    @Override
    public void onBackPressed() {
        if (!activity_from.equals("DialogCreateActivity")) {
            super.onBackPressed();
        }
        else {
            Intent toDialogs = new Intent(DialogActivity.this, MainActivity.class);
            startActivity(toDialogs);
        }
    }

    public void sendMessage(View view) {

        connect_DB.join_test();
        /*
        ObservableTesting source = new ObservableTesting();
        Observer observ = new ObserverTesting();
        source.addObserver(observ);
        source.changeSomething();
        */

        Map message = new HashMap();
        String text = messageInput.getText().toString();
        Date date = new Date();
        Long timestamp = date.getTime()/1000; // timestamp in seconds
        from_me_flag = 1;
        message.put(SQLiteDB.MESSAGE_CHAT_ID_FK, chat_ID);
        message.put(SQLiteDB.MESSAGE_TEXT_MESSAGE, text);
        message.put(SQLiteDB.MESSAGE_TIME, timestamp);
        message.put(SQLiteDB.MESSAGE_FROM_ME, from_me_flag);
        // Время отправки и доставки сообщения фиксируеться на сервере, вариант представленный здесь
        // чисто для тестирования.
        messages.add(message);
        sAdapter.notifyDataSetChanged();
        messageInput.setText("");
        //Log.e("MESSAGE: ", message.toString());
    }

}
