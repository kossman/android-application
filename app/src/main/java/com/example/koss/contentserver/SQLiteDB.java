package com.example.koss.contentserver;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class SQLiteDB extends SQLiteOpenHelper implements BaseColumns {

    // имя базы данных
    private static final String DATABASE_NAME = "mydatabase.db";
    // версия базы данных
    private static final int DATABASE_VERSION = 3; // Добавлено поле receiver_name

    private static final String ID = "_id";

    public static final String CHAT_NAME_TABLE = "chat";
    public static final String CHAT_RECEIVER_ID_FK = "receiver_ID_fk";
    public static final String CHAT_LAST_MESSAGE = "last_message_ID_fk";
    public static final String CHAT_RECEIVER_NAME = "receiver_name";

    public static final String MESSAGE_NAME_TABLE = "message";
    public static final String MESSAGE_CHAT_ID_FK = "chat_ID_fk";
    public static final String MESSAGE_TEXT_MESSAGE = "text";
    public static final String MESSAGE_ATTACH_ID_FK = "attach_ID_fk";
    public static final String MESSAGE_TIME = "time";
    public static final String MESSAGE_FROM_ME = "from_me";

    private static final String CHAT_TABLE_CREATE_SCRIPT = "create table "
            + CHAT_NAME_TABLE + " (" + BaseColumns._ID
            + " integer primary key autoincrement, " + CHAT_RECEIVER_ID_FK
            + " integer not null, " + CHAT_LAST_MESSAGE + " integer not null, " + CHAT_RECEIVER_NAME
            + " text not null, " + "FOREIGN KEY(" + CHAT_LAST_MESSAGE +
            ") REFERENCES " + MESSAGE_NAME_TABLE + "(" + _ID + ")" + ");";

    private static final String MESSAGE_TABLE_CREATE_SCRIPT = "create table "
            + MESSAGE_NAME_TABLE + " (" + BaseColumns._ID
            + " integer primary key autoincrement, " + MESSAGE_CHAT_ID_FK
            + " integer not null, " + MESSAGE_TEXT_MESSAGE + " text," + MESSAGE_ATTACH_ID_FK +
            " integer," + MESSAGE_TIME + " integer not null, " + MESSAGE_FROM_ME + " integer not null, " + "FOREIGN KEY(" + MESSAGE_CHAT_ID_FK +
            ") REFERENCES " + CHAT_NAME_TABLE + "(" + _ID + ")" + ");";

    // constructor
    SQLiteDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(CHAT_TABLE_CREATE_SCRIPT);
        db.execSQL(MESSAGE_TABLE_CREATE_SCRIPT);
        String sql_insert_message = "INSERT or replace INTO " + MESSAGE_NAME_TABLE + "("+ MESSAGE_CHAT_ID_FK + ", "+MESSAGE_TEXT_MESSAGE+", "+MESSAGE_TIME+", "+MESSAGE_FROM_ME+") VALUES('0','Hello!','1476797000','0')";
        String sql_insert_chat = "INSERT or replace INTO " + CHAT_NAME_TABLE + "("+ CHAT_RECEIVER_ID_FK + ", "+CHAT_LAST_MESSAGE+", "+ CHAT_RECEIVER_NAME +") VALUES('0','1','Дмитрий')";
        db.execSQL(sql_insert_message);
        db.execSQL(sql_insert_chat);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion == newVersion) {
            return;
        }
        if (oldVersion < newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + CHAT_NAME_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + MESSAGE_NAME_TABLE);
            onCreate(db);
        }

    }

    public ArrayList<Map<String, String>> getAllChats() {
        /**
         * Structure returned arraylist:
         *      '_id'
         *      'receiver_ID_fk'
         *      'name'
         *      'last_message'
         *
         **/
        String select_join_query = "SELECT c._id as _id, c.receiver_ID_fk as receiver_ID_fk, c.receiver_name as name, m.text as last_message FROM chat AS c INNER JOIN message AS m ON c.last_message_ID_fk = m._id";
        Cursor cursor = this.getWritableDatabase().rawQuery(select_join_query, null);
        //Log.e("cursor value: ", DatabaseUtils.dumpCursorToString(cursor2));

        ArrayList<Map<String, String>> rowsArray = new ArrayList<>();
        Map<String, String> item;

        while (cursor.moveToNext()) {
            item = new HashMap<>();
            Log.e("CURSOR: ", Integer.toString(cursor.getColumnIndex(ID)));
            Integer id = cursor.getInt(cursor.getColumnIndex(ID));
            item.put(ID, id.toString());    // 'id' means '_id'
            Integer receiver_ID_fk = cursor.getInt(cursor.getColumnIndex(CHAT_RECEIVER_ID_FK));
            item.put(CHAT_RECEIVER_ID_FK, receiver_ID_fk.toString());
            String receiver_name = cursor.getString(cursor.getColumnIndex("name"));
            item.put("name", receiver_name);
            String last_message = cursor.getString(cursor.getColumnIndex("last_message"));
            item.put("last_message", last_message);
            rowsArray.add(item);
        }

        cursor.close();
        return rowsArray;
    }

    public ArrayList<Map<String, String>> getAllMessagesChat (Integer chat_ID) {
        Cursor cursor = this.getWritableDatabase().query(MESSAGE_NAME_TABLE, new String[]{ID, MESSAGE_CHAT_ID_FK,
                                                        MESSAGE_TEXT_MESSAGE, MESSAGE_ATTACH_ID_FK, MESSAGE_TIME, MESSAGE_FROM_ME},
                null,
                null,
                null,
                null,
                null);
        ArrayList<Map<String, String>> rowsArray = new ArrayList<>();
        Map<String, String> item;

        while (cursor.moveToNext()) {
            item = new HashMap<>();
            Integer id = cursor.getInt(cursor.getColumnIndex(ID));
            item.put(ID, id.toString());    // 'id' means '_id'
            Integer receiver_ID_fk = cursor.getInt(cursor.getColumnIndex(MESSAGE_CHAT_ID_FK));
            item.put(MESSAGE_CHAT_ID_FK, receiver_ID_fk.toString());
            String last_message = cursor.getString(cursor.getColumnIndex(MESSAGE_TEXT_MESSAGE));
            item.put(MESSAGE_TEXT_MESSAGE, last_message);
            Integer attach_ID = cursor.getInt(cursor.getColumnIndex(MESSAGE_ATTACH_ID_FK));
            item.put(MESSAGE_ATTACH_ID_FK, last_message);
            String message_time = cursor.getString(cursor.getColumnIndex(MESSAGE_TIME));
            item.put(MESSAGE_TIME, last_message);
            String message_from_me = cursor.getString(cursor.getColumnIndex(MESSAGE_FROM_ME));
            item.put(MESSAGE_FROM_ME, message_from_me);
            rowsArray.add(item);
        }
        cursor.close();
        return rowsArray;
    }

    public Boolean checkReceiverID(int receiver_ID) {
        String query = "SELECT * FROM " + CHAT_NAME_TABLE + " where " + CHAT_RECEIVER_ID_FK + " = " + receiver_ID;
        Cursor cursor = this.getReadableDatabase().rawQuery(query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public ArrayList<String> getAllChatIDs () {
        Cursor cursor = this.getWritableDatabase().query(CHAT_NAME_TABLE, new String[]{ID},
                null,
                null,
                null,
                null,
                null);
        ArrayList<String> rowsArray = new ArrayList<>();
        while (cursor.moveToNext()) {

            Integer id = cursor.getInt(cursor.getColumnIndex(ID));
            rowsArray.add(id.toString());
        }
        cursor.close();
        return rowsArray;
    }

    public void join_test() {
        String select_join_query = "SELECT c._id as id, c.receiver_ID_fk as receiver_ID_fk, c.receiver_name as name, m.text as text FROM chat AS c INNER JOIN message AS m ON c.last_message_ID_fk = m._id";
        Cursor cursor = this.getWritableDatabase().rawQuery(select_join_query, null);
        while (cursor.moveToNext()) {
            Log.e("CURSOR: ", cursor.getString(cursor.getColumnIndex("text")));
        }
    }
}
