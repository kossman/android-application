package com.example.koss.contentserver;

import java.io.IOException;
import java.net.Socket;

public class CreateSocket {

    private Socket sock;

    public Socket connect(final String server_address, final Integer server_port) throws IOException {
        Thread thread = new Thread(new Runnable() {
            public void run() {
                try {
                    sock = new Socket(server_address, server_port);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        try {
            thread.join();
        }    // why? ? I don't know!
        catch (InterruptedException e) {
        }   // why? ? I don't know!
        return sock;
    }

}
