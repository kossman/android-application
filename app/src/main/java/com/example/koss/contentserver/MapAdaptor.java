package com.example.koss.contentserver;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MapAdaptor extends SimpleAdapter {

    LayoutInflater lInflater;
    Context ctx;

    public MapAdaptor(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) {
        super(context, data, resource, from, to);
        ctx = context;
        lInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;

        if (view == null) {
            view = lInflater.inflate(R.layout.message_view_item, parent, false);
        }

        Object item = getItem(position);
        String stringValues = item.toString();
        MainActivity.parseMap(stringValues).get("from_me");

        if (MainActivity.parseMap(stringValues).get("from_me").equals("1")) {
            //Log.e("FROM_ME: ", "YES");
            ((TextView) view.findViewById(R.id.outputMessage)).setText(MainActivity.parseMap(stringValues).get("text"));
        }
        else {
            //Log.e("FROM_ME: ", "NO");
            ((TextView) view.findViewById(R.id.inputMessage)).setText(MainActivity.parseMap(stringValues).get("text"));
        }

        return view;
        //return super.getView(position, convertView, parent);
    }
}
