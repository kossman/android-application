package com.example.koss.contentserver;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    public volatile Socket sock;
    public SQLiteDatabase slitedb;
    public SQLiteDB connect_DB;
    public Context current;
    public ArrayList chats;
    private String TAG = "Жизненный цикл";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        connect_DB = new SQLiteDB(this);
        ListView listView = (ListView)findViewById(R.id.list_view);
        current = getBaseContext();
        chats = connect_DB.getAllChats();    // Извлекаем все активные чаты для юзера
        String[] from = {"name", "last_message"};
        int[] to = {R.id.receiver_name_adapter, R.id.last_message_adapter};

        SimpleAdapter sAdapter = new SimpleAdapter(this, chats, R.layout.chat_view_item,
                from, to);

        listView.setAdapter(sAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position,
                                    long id) {
                Intent dialogOpen = new Intent(MainActivity.this, DialogActivity.class);
                dialogOpen.putExtra("activity","MainActivity");
                dialogOpen.putExtra("receiver_ID_fk", Integer.parseInt(parseMap(chats.get(position).toString()).get(SQLiteDB.CHAT_RECEIVER_ID_FK)));
                dialogOpen.putExtra("chat_ID", Integer.parseInt(parseMap(chats.get(position).toString()).get(SQLiteDB._ID)));
                dialogOpen.putExtra("name", parseMap(chats.get(position).toString()).get("name"));
                startActivity(dialogOpen);
            }
        });
    }

    public void onSocketCreateButton(View view) throws IOException {
        final String server_address = getString(R.string.SERVER_ADDR_WIFI);
        final Integer server_port = Integer.parseInt(getString(R.string.SERVER_PORT));

        CreateSocket socket_obj = new CreateSocket();
        sock = socket_obj.connect(server_address, server_port);
        System.out.print(sock.isConnected());
        //text.setText(sock.getInetAddress().toString());
    }

    public void createDialog(View view) {
        Intent dialogCreator = new Intent(MainActivity.this, DialogCreateActivity.class);
        startActivity(dialogCreator);
    }

    public static Map<String,String> parseMap(String text) {    // Жесточайший костыль в истории программирования!!!
        Map<String, String> map = new LinkedHashMap<String, String>();
        for (String keyValue : text.substring(1, text.length() - 1).split(", ")) {
            String[] parts = keyValue.split("=", 2);
            map.put(parts[0], parts[1]);
        }
        return map;
    }

}